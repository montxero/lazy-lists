(defvar *debug-stream* nil)

(defun memo-proc (proc)
  (let (evaluated-p result)
    (lambda ()
      (unless evaluated-p
        (when *debug-stream* (format t "forcing !~%"))
        (setf result (funcall proc))
        (setf evaluated-p t))
      result)))


(defmacro delay (expr)
  `(memo-proc (lambda () ,expr)))


(defmacro lazy-cons (a b)
  `(cons (delay ,a) (delay ,b)))


(defun lazy-car (xs)
  (funcall (car xs)))


(defun lazy-cdr (xs)
  (funcall (cdr xs)))


(defun force (thunk)
  (funcall thunk))


(defun terms (n)
  (lazy-cons (/ 1 (* n n)) (terms (1+ n))))


(defun lazy-list-end-p (xs)
  (eq xs nil))


(defun lazy-list-to-proper-list (xs)
  (labels ((collect (zs acc)
             (let ((acc (cons (lazy-car zs) acc))
                   (zss (lazy-cdr zs)))
               (if (null zss)
                   (nreverse acc)
                   (collect zss acc)))))
    (collect xs nil)))


(defun drop (n lazy-list)
  "Return a lazy list consisting of lazy-list with the first n elements removed"
  (declare (type integer n))
  (when (minusp n) (error "Cannot drop a negative number of elements."))
  (if (zerop n)
      lazy-list
      (drop (1- n) (lazy-cdr lazy-list))))


(defun take (n lazy-list)
  "Return a lazy list consting of the first n elements of lazy-list"
  (declare (type integer n))
  (when (minusp n) (error "Cannot take a negative number of elements."))
  (if (zerop n)
      nil
      (lazy-cons (lazy-car lazy-list) (take (1- n) (lazy-cdr lazy-list)))))


(defun lazy-list-map (fn xs)
  (when xs
    (lazy-cons (funcall fn (lazy-car xs)) (lazy-list-map fn (lazy-cdr xs)))))


(defun foldl (fn acc xs)
  (if (lazy-list-end-p xs)
      acc
      (foldl fn (funcall fn acc (lazy-car xs)) (lazy-cdr xs))))


(defun foldr (fn acc xs)
  (if (lazy-list-end-p xs)
      acc
      (funcall fn (lazy-car xs) (foldr fn acc (lazy-cdr xs)))))


(defun lazy-list-concatenate-2 (xs ys)
  (foldr (lambda (x y) (lazy-cons x y)) ys xs ))


(defun lazy-list-reverse (xs)
  (foldl (lambda (acc x) (lazy-cons x acc)) nil xs))


(defun nats-from (n)
  "Return a lazy list of natural numbers starting from n"
  (lazy-cons n (nats-from (1+ n))))


(defun filter-foldr (test xs)
  (foldr (lambda (x acc) (if (funcall test x) (lazy-cons x acc) acc)) nil xs))

(defun filter-foldl (test xs)
  (lazy-list-reverse (foldl (lambda (acc x) (if (funcall test x) (lazy-cons x acc) acc)) nil xs)))


(defun lazy-list-filter (test xs)
  (when xs
    (let ((x (lazy-car xs)))
      (if (funcall test x)
          (lazy-cons x (lazy-list-filter test (lazy-cdr xs)))
          (lazy-list-filter test (lazy-cdr xs))))))

(defun lazy-list-foldl (fn acc xs)
  (delay (lazy-list-foldl fn (funcall fn acc (lazy-car xs)) (lazy-cdr xs))))

#||


primes = filterPrime [2..]
  where filterPrime (p:xs) =
          p : filterPrime [x | x <- xs, x `mod` p /= 0]

filterPrime (p:xs) = p : filterPrime [x | filter (\x -> (mod x p) /= 0) xs]
filterPrime (p:xs) = p : filterPrime (filter (\x -> (mod x p) /= 0))

||#

(defun primes ()
  (labels ((filter-prime (xs)
             (let ((p (lazy-car xs)))
               (lazy-cons p
                          (filter-prime (lazy-list-filter (lambda (x) (not (zerop (mod x p))))
                                                          (lazy-cdr xs)))))))
    (filter-prime (nats-from 2))))
